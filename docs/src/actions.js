export function toggleTheme(theme) {

  return {type: 'TOGGLE_THEME', theme}
}

export function toggleMode(mode) {

  return {type: 'TOGGLE_MODE', mode}
}

export function toggleExample(example) {

  return {type: 'TOGGLE_EXAMPLE', example}
}

export function toggleServer(server) {

  return {type: 'TOGGLE_SERVER', server}
}

export function toggleCodeFa(codeFa) {

  return {type: 'TOGGLE_CODEFA', codeFa}
}

export function toggleCodeCpp(codeCpp) {

  return {type: 'TOGGLE_CODECPP', codeCpp}
}

export function toggleOutput(output) {

  return {type: 'TOGGLE_OUTPUT', output}
}

export function toggleData(data) {

  return {type: 'TOGGLE_DATA', data}
}

import React from 'react';
import EditorFa from './EditorFa.jsx'
import EditorCpp from './EditorCpp.jsx'
import Controls from './Controls.jsx';
import Syntax from './Syntax.jsx';
import AppHeader from "./AppHeader.jsx";
import OutputBlock from "./OutputBlock.jsx";
import "./AppBody.css";
import TableBlock from "./TableBlock.jsx";
import NavDropdown from "react-bootstrap/cjs/NavDropdown";
import Button from "react-bootstrap/cjs/Button";
import {Form} from "react-bootstrap";


export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      example: 'exampleHello',
      server: 'lunaWebServer',
      output: '',
      data: []
    }
  }

  render() {

    return (
      <div id='container'>
        <AppHeader server={this.state.server}/>
        <div id='pane-container'>
          <section className='block'>
            <h3>test.fa</h3>
            <EditorFa example={this.state.example}/>
          </section>
          <section className='block'>
            <h3>ucodes.cpp</h3>
            <EditorCpp example={this.state.example}/>
          </section>
          <section className='block'>
            <h3>Output</h3>
            <OutputBlock output={this.state.output} />
          </section>
          <section className='block'>
            <TableBlock data={this.state.data}/>
          </section>
        </div>
      </div>
    );
  }
}

import React from 'react';
import {connect} from 'react-redux';

import {Controlled} from '../../../index.js'
import store from "../store";
import * as actions from "../actions";

let jBeautify = require('js-beautify').js;

// http://marijnhaverbeke.nl/blog/codemirror-mode-system.html
let sampleMode = () => {
  return {
    startState: function () {
      return {inString: false};
    },
    token: function (stream, state) {
      // If a string starts here
      if (!state.inString && stream.peek() == '"') {
        stream.next();            // Skip quote
        state.inString = true;    // Update state
      }

      if (state.inString) {
        if (stream.skipTo('"')) { // Quote found on this line
          stream.next();          // Skip quote
          state.inString = false; // Clear flag
        } else {
          stream.skipToEnd();    // Rest of line is string
        }
        return "string";          // Token style
      } else {
        stream.skipTo('"') || stream.skipToEnd();
        return null;              // Unstyled token
      }
    }
  };
};

class EditorCpp extends React.Component {

  constructor(props) {
    super(props);

    let exampleHello = '#include <cstdio>\n' +
      '\n' +
      'extern "C"\n' +
      'void c_helloworld() {\n' +
      '\tprintf("Hello world!\\n");\n' +
      '}\n';
    this.defaultExampleHello = jBeautify(exampleHello, {indent_size: 4});

    let example1 = '#include <cstdio>\n' +
      '#include "ucenv.h"\n' +
      '\n' +
      'extern "C" {\n' +
      '\n' +
      'void c_init(int val, OutputDF &df) {\n' +
      '\tdf.setValue(val);\n' +
      '\tprintf("c_init: %d, size: %d\\n", val, (int)df.getSize());\n' +
      '}\n' +
      '\n' +
      'void c_iprint(int val) {\n' +
      '\tprintf("c_iprint %d\\n", val);\n' +
      '}\n' +
      '\n' +
      'void c_print(const InputDF &df) {\n' +
      '\tprintf("c_print: %d\\n", df.getValue<int>());\n' +
      '}\n' +
      '\n' +
      '}\n';
    this.defaultExample1 = jBeautify(example1, {indent_size: 4});

    this.state = {
      value: this.defaultExampleHello
    };
    store.dispatch(actions.toggleCodeCpp(this.state.value));
  }

  componentDidUpdate(prevProps, prevState) {

    if (this.props.example && !prevProps.example) {
      this.setValue(this.props.example);
    }

    if (this.props.example && (this.props.example !== prevProps.example)) {
      this.setValue(this.props.example);
    }
  }

  setValue(example) {
    switch (example) {
      case 'exampleHello':
        this.setState({value: this.defaultExampleHello});
        break;
      case 'example1':
        this.setState({value: this.defaultExample1});
        break;
    }
  }

  render() {
    return (
      <Controlled
        value={this.state.value}
        defineMode={{name: 'strings', fn: sampleMode}}
        options={{
          mode: "javascript",
          theme: this.props.theme,
          lineNumbers: true
        }}
        onBeforeChange={(editor, data, value) => {
          this.setState({value});
        }}
        onChange={(editor, data, value) => {
          store.dispatch(actions.toggleCodeCpp(value));
        }}
      />
    )
  }
}

function mapState(state) {
  return {
    theme: state.app.theme,
    mode: state.app.mode,
    example: state.app.example
  }
}

export default connect(mapState)(EditorCpp)

import Navbar from "react-bootstrap/cjs/Navbar";
import Nav from "react-bootstrap/cjs/Nav";
import {Form} from "react-bootstrap";
import Button from "react-bootstrap/cjs/Button";
import React from "react";
import NavDropdown from "react-bootstrap/cjs/NavDropdown";
import * as actions from '../actions.js';
import {connect} from 'react-redux';
import store from "../store.js";
import axios from 'axios';
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/cjs/Row";
import Col from "react-bootstrap/cjs/Col";

class AppHeader extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      example: 'exampleHello',
      server: 'lunaWebServer',
      title: 'LuNA Web Server',
      codeCpp: '',
      codeFa: '',
      show: false,
      numThreads: 1
    }

    this.onRunClick = this.onRunClick.bind(this);
    this.onModalOpenClick = this.onModalOpenClick.bind(this);
    this.onModalCloseClick = this.onModalCloseClick.bind(this);
    this.onChangeThreads = this.onChangeThreads.bind(this);
  }

  onModalOpenClick() {
    this.setState({show: true})
  }

  onModalCloseClick() {
    this.setState({show: false})
  }

  onExampleSelect(example) {
    store.dispatch(actions.toggleExample(example));
  }

  onServerSelect(server) {
    store.dispatch(actions.toggleServer(server));
  }

  onChangeThreads(event) {
    this.setState({numThreads: event.target.value})
  }

  componentDidUpdate(prevProps, prevState) {

    // Обновление варианта сервера
    if (this.props.server && !prevProps.server) {
      this.setTitleServer(this.props.server);
    }

    if (this.props.server && (this.props.server !== prevProps.server)) {
      this.setTitleServer(this.props.server);
    }

    // Обновление текста исходного кода LuNA
    if (this.props.codeFa && !prevProps.codeFa) {
      this.setCodeFa(this.props.codeFa);
    }

    if (this.props.codeFa && (this.props.codeFa !== prevProps.codeFa)) {
      this.setCodeFa(this.props.codeFa);
    }

    // Обновление текста исходного кода Cpp
    if (this.props.codeCpp && !prevProps.codeCpp) {
      this.setCodeCpp(this.props.codeCpp);
    }

    if (this.props.codeCpp && (this.props.codeCpp !== prevProps.codeCpp)) {
      this.setCodeCpp(this.props.codeCpp);
    }

    if (this.props.server && !prevProps.server) {
      this.setTitleServer(this.props.server);
    }

    if (this.props.server && (this.props.server !== prevProps.server)) {
      this.setTitleServer(this.props.server);
    }
  }

  setTitleServer(server) {
    switch (server) {
      case 'lunaWebServer':
        this.setState({title: 'LuNa Web Server'});
        break;
      case 'hpcCloud':
        this.setState({title: 'HPC Cloud'});
        break;
      case 'webRTS':
        this.setState({title: 'Web RTS'});
        break;
    }
  }

  setCodeFa(code) {
    this.setState({codeFa: code})
  }

  setCodeCpp(code) {
    this.setState({codeCpp: code})
  }

  onRunClick() {
    this.setState({show: false})
    // 1. Creating project
    const projectDto = {
      type: 'string',
      name: 'test_front',
      template: 'luna',
      cluster_profile: '0',
      make_configuration: '0'
    }
    axios.post('http://localhost:8080/api/projects',
      projectDto,
      {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      })
      .then(res => {
        console.log(res)
      })

    // 2. Creating files
    const fileFaDto = {
      path: 'app/test_front/test.fa',
      file: 'file',
      body: this.state.codeFa
    }
    const fileCppDto = {
      path: 'app/test_front/ucodes.cpp',
      file: 'file',
      body: this.state.codeCpp
    }
    console.log(this.state.codeFa)
    console.log(this.state.codeCpp)
    axios.post('http://localhost:8080/api/fs', fileFaDto)
      .then(() => {
      })
    axios.post('http://localhost:8080/api/fs', fileCppDto)
      .then(() => {
      })

    console.log(this.state.numThreads)

    // 3. Creating job
    const jobDto = {
      name: '',
      type: '',
      cluster_profile_id: '',
      state: '1',
      chunks:
        [
          {
            count: 1,
            resources: [
              {
                name: 'ncpus',
                value: '4'
              },
              {
                name: 'mpiprocs',
                value: this.state.numThreads
              },
            ]
          }
        ],
      walltime: '1',
      executable_path: 'app/test_front/test.fa',
      args: '1',
      env_vars: '1',
      queue_name: ''
    }
    axios.post('http://localhost:8080/api/jobs', jobDto)
      .then(res => {
        console.log(res)

        // 4. Run Job
        axios.post('http://localhost:8080/api/jobs/' + res.data + '/run')
          .then(res => {
            let arrayTime = JSON.parse(sessionStorage.getItem('arrayTime'));

            if (!arrayTime) {
              arrayTime = []
            }
            console.log(res)
            const item = {
              name: (arrayTime.length + 1).toString(),
              value: Number.parseFloat(res.data.time)
            }
            arrayTime.push(item)

            sessionStorage.setItem('arrayTime', JSON.stringify(arrayTime));

            const out = res.data.body.replace('[2K\n', '')
            store.dispatch(actions.toggleOutput(out.replaceAll('[2K\n', '\n')));
            store.dispatch(actions.toggleData(item));
          })
      })
  }

  render() {
    return <>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="/home">LuNA</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="/home">Home</Nav.Link>
          <Nav.Link href="/documentation">Documentation</Nav.Link>
          <NavDropdown title="Examples" id="basic-nav-dropdown" onSelect={this.onExampleSelect}>
            <NavDropdown.Item eventKey={'exampleHello'}>Hello world!</NavDropdown.Item>
            <NavDropdown.Item eventKey={'example1'}>Example 1</NavDropdown.Item>
          </NavDropdown>
        </Nav>
        <Form inline>
          <NavDropdown title={this.state.title} id="basic-nav-dropdown" onSelect={this.onServerSelect}>
            <NavDropdown.Item eventKey={'lunaWebServer'}>LuNa Web Server</NavDropdown.Item>
            <NavDropdown.Item eventKey={'hpcCloud'}>HPC Cloud</NavDropdown.Item>
            <NavDropdown.Item eventKey={'webRTS'}>Web RTS</NavDropdown.Item>
          </NavDropdown>
          <Button variant="outline-info" onClick={this.onRunClick}>Run</Button>
        </Form>
      </Navbar>

      <Modal show={this.state.show} animation={true} onHide={this.onModalCloseClick}>
        <Modal.Header>
          <Modal.Title>Variables</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Row className="align-items-center">
                <Col sm={6}>
                  <Form.Label>Number of threads</Form.Label>
                </Col>
                <Col sm={6}>
                  <Form.Control value={this.state.numThreads} onChange={this.onChangeThreads} type="number"/>
                </Col>
              </Row>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.onModalCloseClick}>
            Close
          </Button>
          <Button variant="primary" onClick={this.onRunClick}>
            Run
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  }
}

function mapState(state) {
  return {
    theme: state.app.theme,
    mode: state.app.mode,
    example: state.app.example,
    server: state.app.server,
    codeFa: state.app.codeFa,
    codeCpp: state.app.codeCpp
  }
}

export default connect(mapState)(AppHeader)

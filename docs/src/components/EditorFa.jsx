import React from 'react';
import {connect} from 'react-redux';

import {Controlled} from '../../../index.js'
import store from "../store";
import * as actions from "../actions";

let jBeautify = require('js-beautify').js;

// http://marijnhaverbeke.nl/blog/codemirror-mode-system.html
let sampleMode = () => {
  return {
    startState: function () {
      return {inString: false};
    },
    token: function (stream, state) {
      // If a string starts here
      if (!state.inString && stream.peek() == '"') {
        stream.next();            // Skip quote
        state.inString = true;    // Update state
      }

      if (state.inString) {
        if (stream.skipTo('"')) { // Quote found on this line
          stream.next();          // Skip quote
          state.inString = false; // Clear flag
        } else {
          stream.skipToEnd();    // Rest of line is string
        }
        return "string";          // Token style
      } else {
        stream.skipTo('"') || stream.skipToEnd();
        return null;              // Unstyled token
      }
    }
  };
};

class EditorFa extends React.Component {

  constructor(props) {
    super(props);

    let exampleHello = '/*\n' +
      ' Hello world example.\n' +
      '*/\n' +
      '\n' +
      'import c_helloworld() as hello_world;\n' +
      '\n' +
      'sub main()\n' +
      '{\n' +
      '\thello_world() @ { locator_cyclic: 0; };\n' +
      '}\n';
    this.defaultExampleHello = jBeautify(exampleHello, {indent_size: 4});

    let example1 = '/*\n' +
      ' Initialization of data fragments and data dependencies.\n' +
      '*/\n' +
      '\n' +
      'import c_init(int, name) as init;\n' +
      'import c_iprint(int) as iprint;\n' +
      '\n' +
      'sub main()\n' +
      '{\n' +
      '\tdf x;\n' +
      '\n' +
      '\tcf a: init(7, x) @ {\n' +
      '//\t\treq_count x=1;\n' +
      '\t\tlocator_cyclic: 1;\n' +
      '\t};\n' +
      '\n' +
      '\tiprint(x) @ {\n' +
      '\t\trequest x;\n' +
      '\t\tlocator_cyclic: 2;\n' +
      '\t};\n' +
      '\n' +
      '} @ {\n' +
      '\tlocator_cyclic x => 3;\n' +
      '}\n';
    this.defaultExample1 = jBeautify(example1, {indent_size: 4});

    this.state = {
      value: this.defaultExampleHello
    };
    store.dispatch(actions.toggleCodeFa(this.state.value));
  }

  componentDidUpdate(prevProps, prevState) {

    if (this.props.example && !prevProps.example) {
      this.setValue(this.props.example);
    }

    if (this.props.example && (this.props.example !== prevProps.example)) {
      this.setValue(this.props.example);
    }
  }

  setValue(example) {
    switch (example) {
      case 'exampleHello':
        this.setState({value: this.defaultExampleHello});
        break;
      case 'example1':
        this.setState({value: this.defaultExample1});
        break;
    }
  }

  render() {
    return (
      <Controlled
        value={this.state.value}
        defineMode={{name: 'strings', fn: sampleMode}}
        options={{
          mode: "javascript",
          theme: this.props.theme,
          lineNumbers: true
        }}
        onBeforeChange={(editor, data, value) => {
          this.setState({value});
        }}
        onChange={(editor, data, value) => {
          store.dispatch(actions.toggleCodeFa(value));
        }}
      />
    )
  }
}

function mapState(state) {
  return {
    theme: state.app.theme,
    mode: state.app.mode,
    example: state.app.example
  }
}

export default connect(mapState)(EditorFa)

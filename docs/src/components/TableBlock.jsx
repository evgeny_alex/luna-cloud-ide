import React from "react";
import "./AppBody.css";
import * as d3 from "d3";
import {connect} from "react-redux";
import Button from "react-bootstrap/cjs/Button";
import Row from "react-bootstrap/cjs/Row";
import Col from "react-bootstrap/cjs/Col";

let data = (JSON.parse(sessionStorage.getItem('arrayTime')) ? JSON.parse(sessionStorage.getItem('arrayTime')) : [])

let getY = d3.scaleLinear()
  .domain([0, Math.ceil(Math.max.apply(null, data.map(item => item.value)))])
  .range([300, 0]);

let getX = d3.scaleBand()
  .domain(data.map(it => it.name))
  .range([0, 600]);

let getYAxis = ref => {
  const yAxis = d3.axisLeft(getY);
  d3.select(ref).call(yAxis);
};

let getXAxis = ref => {
  const xAxis = d3.axisBottom(getX);
  d3.select(ref).call(xAxis);
};

let linePath = d3
  .line()
  .x(d => getX(d.name) + getX.bandwidth() / 2)
  .y(d => getY(d.value))
  .curve(d3.curveMonotoneX)(data);

let areaPath = d3.area()
  .x(d => getX(d.name) + getX.bandwidth() / 2)
  .y0(d => getY(d.value))
  .y1(() => getY(0))
  .curve(d3.curveMonotoneX)(data);

class TableBlock extends React.Component {

  constructor() {
    super();

    this.state = {
      activeIndex: null,
      data: (JSON.parse(sessionStorage.getItem('arrayTime')) ? JSON.parse(sessionStorage.getItem('arrayTime')) : [])
    }

    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
    this.setData = this.setData.bind(this);
    this.onClearClick = this.onClearClick.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // Обновление данных таблицы

    if (this.props.data && !prevProps.data) {
      this.setData(this.props.data);
    }

    if (this.props.data && (this.props.data !== prevProps.data)) {
      this.setData(this.props.data);
    }
  }

  setData(item) {
    let newData = this.state.data

    if (item) {
      newData.push(item)
      this.setState({data: newData})
    } else {
      newData = []
    }

    getY = d3.scaleLinear()
      .domain([0, Math.ceil(Math.max.apply(null, newData.map(item => item.value)))])
      .range([300, 0]);

    getX = d3.scaleBand()
      .domain(newData.map(it => it.name))
      .range([0, 600]);

    getYAxis = ref => {
      const yAxis = d3.axisLeft(getY);
      d3.select(ref).call(yAxis);
    };

    getXAxis = ref => {
      const xAxis = d3.axisBottom(getX);
      d3.select(ref).call(xAxis);
    };

    linePath = d3
      .line()
      .x(d => getX(d.name) + getX.bandwidth() / 2)
      .y(d => getY(d.value))
      .curve(d3.curveMonotoneX)(newData);

    areaPath = d3.area()
      .x(d => getX(d.name) + getX.bandwidth() / 2)
      .y0(d => getY(d.value))
      .y1(() => getY(0))
      .curve(d3.curveMonotoneX)(newData);
  }

  handleMouseMove(e) {
    const x = e.nativeEvent.offsetX; // количество пикселей от левого края svg
    const index = Math.floor(x / getX.step()); // делим количество пикселей на ширину одной колонки и получаем индекс
    this.setState({activeIndex: index}); // обновляем наше состояние
  };

  handleMouseLeave() {
    this.setState({activeIndex: null}); // обновляем наше состояние
  };

  onClearClick() {
    sessionStorage.setItem('arrayTime', JSON.stringify([]));
    this.setState({data: []})
    this.setData(null)
  }


  render() {
    return (
      <div>
        <Row className="show-grid">
          <Col md={10}>
            <h3>Table</h3>
          </Col>
          <Col md={2}>
            <Button variant="outline-info" onClick={this.onClearClick}>Clear</Button>
          </Col>
        </Row>
        <svg width={650} height={400}
             onMouseMove={this.handleMouseMove}
             onMouseLeave={this.handleMouseLeave}>
          <g ref={getYAxis}
             transform={`translate(50,20)`}/>
          <g
            ref={getXAxis}
            transform={`translate(50,320)`} // нужно сдвинуть ось в самый низ svg
          />
          <path
            strokeWidth={3}
            fill="none"
            stroke="#7cb5ec"
            d={linePath}
            transform={`translate(50,20)`}
          />
          <path
            fill="#7cb5ec"
            d={areaPath}
            opacity={0.2}
            transform={`translate(50,20)`}
          />
          {this.state.data.map((item, index) => {
            return (
              <g key={index}
                 transform={`translate(50,20)`}>
                <circle
                  cx={getX(item.name) + getX.bandwidth() / 2}
                  cy={getY(item.value)}
                  r={index === this.state.activeIndex ? 6 : 4} // при наведении просто немного увеличиваем круг
                  fill="#7cb5ec"
                  strokeWidth={index === this.state.activeIndex ? 2 : 0} // обводка появляется только при наведении
                  stroke="#fff" // добавили белый цвет для обводки
                  style={{transition: `ease-out .1s`}}
                />
                <text
                  fill="#666"
                  x={getX(item.name) + getX.bandwidth() / 2}
                  y={getY(item.value) - 10}
                  textAnchor="middle"
                >
                  {item.value}
                </text>
              </g>
            );
          })}
        </svg>
      </div>
    );
  }
}

function mapState(state) {
  return {
    theme: state.app.theme,
    mode: state.app.mode,
    data: state.app.data
  }
}

export default connect(mapState)(TableBlock)

import React from "react";
import {connect} from 'react-redux';

import {Controlled} from '../../../index.js'
import store from "../store";
import * as actions from "../actions";
import "./AppBody.css";

let jBeautify = require('js-beautify').js;

// http://marijnhaverbeke.nl/blog/codemirror-mode-system.html
let sampleMode = () => {
  return {
    startState: function () {
      return {inString: false};
    },
    token: function (stream, state) {
      // If a string starts here
      if (!state.inString && stream.peek() == '"') {
        stream.next();            // Skip quote
        state.inString = true;    // Update state
      }

      if (state.inString) {
        if (stream.skipTo('"')) { // Quote found on this line
          stream.next();          // Skip quote
          state.inString = false; // Clear flag
        } else {
          stream.skipToEnd();    // Rest of line is string
        }
        return "string";          // Token style
      } else {
        stream.skipTo('"') || stream.skipToEnd();
        return null;              // Unstyled token
      }
    }
  };
};

class OutputBlock extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      output: ''
    };
  }

  componentDidUpdate(prevProps, prevState) {

    if (this.props.output && !prevProps.output) {
      this.setState({output: this.props.output});
    }

    if (this.props.output && (this.props.output !== prevProps.output)) {
      this.setState({output: this.props.output});
    }
  }

  render() {
    return (
      <Controlled
        value={this.state.output}
        defineMode={{name: 'strings', fn: sampleMode}}
        options={{
          mode: "javascript",
          theme: this.props.theme,
          lineNumbers: true
        }}
        onBeforeChange={(editor, data, value) => {
          this.setState({value});
        }}
      />
    )
  }
}

function mapState(state) {
  return {
    theme: state.app.theme,
    mode: state.app.mode,
    output: state.app.output
  }
}

export default connect(mapState)(OutputBlock)


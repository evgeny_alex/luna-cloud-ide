import React from "react";
import OutputBlock from "./OutputBlock.jsx";
import Container from "react-bootstrap/cjs/Container";
import Row from "react-bootstrap/cjs/Row";
import Col from "react-bootstrap/cjs/Col";
import "./AppBody.css";
import Editor from "./EditorFa.jsx";

export default class AppBody extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      controlled: false
    }
  }

  render() {
    return <Container fluid={true}>
      <Col>
        <Row>
          <Col>
            <Editor controlled={this.state.controlled}/>
          </Col>
          <Col>
            <Editor controlled={this.state.controlled}/>
          </Col>
        </Row>
      </Col>
    </Container>
  }
}

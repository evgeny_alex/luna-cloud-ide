export default function reducer(state = [], action) {

  switch (action.type) {

    case 'TOGGLE_THEME': {
      return Object.assign({}, state, {theme: action.theme});
    }

    case 'TOGGLE_MODE': {
      return Object.assign({}, state, {mode: action.mode});
    }

    case 'TOGGLE_EXAMPLE': {
      return Object.assign({}, state, {example: action.example});
    }

    case 'TOGGLE_SERVER': {
      return Object.assign({}, state, {server: action.server});
    }

    case 'TOGGLE_CODEFA': {
      return Object.assign({}, state, {codeFa: action.codeFa});
    }

    case 'TOGGLE_CODECPP': {
      return Object.assign({}, state, {codeCpp: action.codeCpp});
    }

    case 'TOGGLE_OUTPUT': {
      return Object.assign({}, state, {output: action.output});
    }

    case 'TOGGLE_DATA': {
      return Object.assign({}, state, {data: action.data});
    }

    default:
      return state;
  }
}

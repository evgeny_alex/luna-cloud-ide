import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, combineReducers} from 'redux';
import reducer from './reducers';
import App from './components/App.jsx';
import store from "./store";

require('bootstrap/dist/css/bootstrap.css');
require('./index.scss');
require('codemirror/mode/xml/xml');
require('codemirror/mode/javascript/javascript');
require('codemirror/mode/php/php');
require('prismjs/components/prism-jsx');

render(
  <Provider store={store}>
    <App/>
  </Provider>
  , document.getElementById('app'));


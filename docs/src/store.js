import {createStore, combineReducers} from 'redux';
import reducer from './reducers';

let initialState = {
  app: {
    theme: 'xq-light',
    mode: 'javascript',
    example: 'exampleHello',
    server: 'lunaWebServer',
    codeFa: '',
    codeCpp: '',
    output: '',
    data: []
  }
};

const app = combineReducers({
  app: reducer
});

const store = createStore(app, initialState);

export default store;
